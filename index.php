<!DOCTYPE html>
<!--TODO: Añadir posibilidad de traducciones -->
<html lang="ES">
<head>
<title>Inicio de Sesion|Freegig</title>
<meta charset="UTF-8">
<!--Compatibilidad con Edge y responsiveness  -->
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- TODO hoja de estilo ad-hoc y buscar fontawesione -->
<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
<link href="css/font-awesome.css" rel="stylesheet">
</head>
<body>
<section class="w3l-form-36">
<div class="form-36-mian section-gap">
<div class="wrapper">
<div class="form-inner-cont">
<h3>Ingrese y comience a obtener ingresos</h3>
<form action="#" method="post" class="signin-form">
<div class="form-input">
<span class="fa fa-envelope-o" aria-hidden="true"></span> <input type="email" name="email" placeholder="Nombre de usuario" required>
</div><div class="form-input">
<span class="fa fa-key" aria-hidden="true"></span> <input type="password" name="password" placeholder="Contraseña" required>
</div>
<div class="login-remember d-grid">
<label class="check-remaind">
<input type="checkbox">
<span class="checkmark"></span>
<p class="remember">No cerrar sesión en este equipo</p>
</label>
<button class="btn theme-button">Iniciar Sesión </button>
</div>
<div class="new-signup">
<a href="forgot.php" class="signuplink">Olvide Mi contraseña</a>
</div>
</form>
<div class="social-icons">
<p class="continue"><span>Or</span></p>
<div class="social-login">
<a href="#facebook">
<div class="facebook">
<span class="fa fa-facebook" aria-hidden="true"></span>
</div>
</a>
<a href="#google">
<div class="google">
<span class="fa fa-google-plus" aria-hidden="true"></span>
</div>
</a>
</div>
</div>
<p class="signup">Que no tienes cuenta?, Entonces  <a href="#signup.html" class="signuplink">Registrate</a></p>
</div>
</div>
</div>
</section>
</body>
</html>
